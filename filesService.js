// requires...

// constants...

const fs = require("fs");
const path = require("path");

function createFile (req, res, next) {
  if (!req.body.filename || !req.body.content) {
    res.status(400).send({"message": "Please specify 'content' parameter"});
  }
 
  fs.writeFile(path.resolve(__dirname, 'files', req.body.filename), req.body.content, function (err) {
    if (err) {
      res.status(500).send({'message': 'Server error'});
    } else {
      res.status(200).send({"message": "File created successfully"});
    }

  })
}

function getFiles (req, res, next) {
  if (!fs.existsSync(path.resolve(__dirname, 'files'))) {
    res.status(400).send({'message': 'Client error'});
  }
  fs.readdir(path.resolve(__dirname, 'files'), ((err, files) => {
    if (err) {
      res.status(500).send({'message': 'Server error'});
    }
    const listOfTheFiles = [];
    files.forEach(file => {
      listOfTheFiles.push(file);
    })
    res.status(200).send({'message': 'Success', 'files': listOfTheFiles});
  }));

}

const getFile = (req, res, next) => {
  // Your code to get all files.

  let fileName = req.params.filename;
  const pathToFile = path.resolve(__dirname, 'files', fileName);
  if (!fs.existsSync(path.resolve(__dirname, 'files', fileName))) {
    res.status(400).send({
      "message": `No file with '${fileName}' filename found`
    })
  } else {
    fs.readFile(pathToFile, 'utf-8', async (err, data) => {
      if (err) {
        res.status(500).send({
          "message": "Server error"
        })
      } else {
        let formatOfTheFile = path.extname(fileName).substring(1);

        res.status(200).send({
          "message": "Success",
          "filename": fileName,
          "content": data,
          "extension": formatOfTheFile,
          "uploadedDate": fs.statSync(pathToFile).birthtime
        });
      }
    })
  }
}
function deleteFile(req,res){
  let fileName = req.params.filename;
  const pathToFile = path.resolve(__dirname, 'files', fileName);
  if (!fs.existsSync(path.resolve(__dirname, 'files', fileName))) {
    res.status(400).send({
      "message": `No file with '${fileName}' filename found`
    })
  } else {
    fs.unlink(pathToFile,(err => {
      if(err){
        res.status(500).send({
          "message": "Server error"
        });
      }
      res.status(200).send({"message":"File removed successfully"});
    }))
  }
}
function modifyFile(req,res){
  let fileName = req.params.filename;
  const pathToFile = path.resolve(__dirname, 'files', fileName);
  if (!req.body.content) {
    res.status(400).send({"message": "Please specify 'content' parameter"});
  }
  if (!fs.existsSync(pathToFile)) {
    res.status(400).send({
      "message": `No file with '${fileName}' filename found`
    })
  } else {
    fs.writeFile(pathToFile,req.body.content,(err)=>{
      if(err){
        res.status(500).send({'message': 'Server error'});
      }
      res.status(200).send({"message": "File changed successfully"});
    })
  }
}
// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  modifyFile
}
